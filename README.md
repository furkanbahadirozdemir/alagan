# Alagan

Android studio içerisinde bizim işlerimizi kolaylaştıracak olan bir paketler topluluğudur. Şuanda içerisinde yanlızca Fountain bulunmaktadır.

# Kurulum

```
allprojects 
{
    repositories 
    {
        …
        maven { url 'https://jitpack.io' }
    }
}

dependencies
{
        implementation 'com.gitlab.furkanbahadirozdemir:alagan:3.0'
}
```
```
<manifest xlmns:android...>
 ...
 
    <application 
        ...
        android:networkSecurityConfig="@xml/network_security_config">...
     
    </application>
</manifest>
```
res\xml\network_security_config.xml
```
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="true">192.168.1.108</domain>
    </domain-config>
</network-security-config>
```

```
<manifest xlmns:android...>
 ...
 <uses-permission android:name="android.permission.INTERNET" />
 <application ...
</manifest>
```

```
<application>
    ...
    <uses-library android:name="org.apache.http.legacy" android:required="false" />
</application>
```

## Butler

Butler Volley kütüphanesini kullanarak daha basit bir şekilde API Server a istek atmamızı sağlayan bir yapıyı bize sunuyor. 


```
 Butler.Instance().Initialize(getApplicationContext(),"http://192.168.1.108:3000");
```
Örnek : "http://192.168.1.108:3000/test:5"
```
ButlerRequest request = Butler.Instance().Path("test").GET("5");
request.SEND(new IButlerListener() 
{
    @Override
    public void OnResponse(String data) 
    {

        Log.d("TutkalResponse",data);
    }
});
```

## Fountain
 
 Fountain, bizim arayüz işlemlerindeki işlerimizi kolaylaştıracak olan modüldür. Mesela her listview için bir adapter açmadan bize dinamik olarak listviewleri kontrol etme şansı verir. Yazdığımız kodların hem karmaşası azalır hemde daha pratik bir şekilde listview oluşturabilirz.
 
 Fountain içersinde olacak olan arayüz elemanları ; 
 1. ListView
 2. RecyclerView
 3. ExpandableListView
 4. Gridview
 5. Spinner
 
 Bunların dışında Fountain ile birlikte kolay bir şekilde animasyon yapabileceksiniz.
 
 
 
  
### ListView

 ```javascript
List<String> data = new ArrayList<>(Arrays.asList("C#","HTML","Java","Unity") ) ;
ListView listView = findViewById(R.id.list);

FountainList fountainList = Fountain.Initialize(getApplicationContext()).List().ListView(listView);
fountainList.SetLayout(R.layout.item).SetItemList(data);
fountainList.SetListener(new IListview<String>() {
    @Override
    public void OnListViewItem(View item, String data) {
        TextView textView = item.findViewById(R.id.tes);
        textView.setText(data);
    }
});
fountainList.Create();
```
Ana listeniz üzerinde herhangi bir değişiklik yaptıktan sonra görüntülenen listenin tekrar düzenlenmesi için bu komutu kullanabilirsiniz ; 
```
data.notifyDataSetChanged();
```
### ExpandableListView
```javascript
FountainExpandableList list = Fountain.Initialize().List().ExpandableListView();
list.SetGroupLayout(R.layout.oneline).SetChildLayout(R.layout.oneline).SetContext(getApplicationContext());


List<String> dataSet1 = new ArrayList<String>(Arrays.asList("C#","Java","Objective-C","Html"));
List<String> dataSet2 = new ArrayList<String>(Arrays.asList("Cocos2dx","Unity","Unreal Engine"));

list.SetItemList("Programming languages",dataSet1 ,"Game Engine", dataSet2);

list.SetListener(new IExpandableView() {
    @Override
    public void OnDrawHeader(View layout, Object data) {
        TextView k = layout.findViewById(R.id.headerText);
        k.setText((String)data);
    }

    @Override
    public void OnDrawItem(View layout, Object data) {
        TextView k = layout.findViewById(R.id.headerText);
        k.setText((String)data);
    }
});
list.Create((ExpandableListView)findViewById(R.id.expandableList));
```
