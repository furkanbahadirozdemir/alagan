package com.furkanbahadirozdemir.butler;

import android.app.Activity;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Butler {

    String URL;
    Context context;
    private static Butler instance;

    List<String> pathURL = new ArrayList<>();

    public static Butler Instance()
    {
        if (instance == null)
            instance = new Butler();


        return instance;
    }

    public Butler Initialize(Context context,String url)
    {
        URL = url;
        this.context = context;
        return instance;
    }

    public Butler Path(String path)
    {
        pathURL.add(path);
        return this;
    }



    public ButlerRequest GET()
    {
        return CreateButlerWorker(Request.Method.GET,null);
    }
    public ButlerRequest GET(String id)
    {
        return   CreateButlerWorker(Request.Method.GET,id);
    }

    public ButlerRequest PUT()
    {
        return CreateButlerWorker(Request.Method.PUT,null);
    }
    public ButlerRequest PUT(String id)
    {
        return CreateButlerWorker(Request.Method.PUT,id);
    }
    public ButlerRequest DELETE()
    {
        return CreateButlerWorker(Request.Method.DELETE,null);
    }
    public ButlerRequest DELETE(String id)
    {
        return CreateButlerWorker(Request.Method.DELETE,id);
    }

    public ButlerRequest POST()
    {
        return CreateButlerWorker(Request.Method.POST,null);
    }
    public ButlerRequest POST(String id)
    {
        return CreateButlerWorker(Request.Method.POST,id);
    }
    private ButlerRequest CreateButlerWorker(int type,String id)
    {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < pathURL.size(); i++) {

            stringBuilder.append(pathURL.get(i)+"/");

        }

        String newURL =  String.valueOf(URL)+"/"+stringBuilder.toString();

        if (id != null)
            newURL += id;

        pathURL.clear();

        return new ButlerRequest(context,newURL,type);
    }

}
