package com.furkanbahadirozdemir.butler;

import android.content.Context;

import com.android.volley.Request;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import java.util.HashMap;
import java.util.Map;

public class ButlerRequest
{
    int requestMethod;
    Context context;
    String URL;
    Map<String,String> params = new HashMap<String, String>();
    public ButlerRequest(Context context,String URL, int requestMethod)
    {
        this.requestMethod = requestMethod;
        this.context = context;
        this.URL = URL;
    }

    public ButlerRequest Params(String key, String values)
    {
        params.put(key,values);
        return this;
    }

    public void SEND()
    {
        ButlerWorker worker = new ButlerWorker(context,params,URL,requestMethod,null);
    }
    public void SEND(IButlerListener listener)
    {
        ButlerWorker worker = new ButlerWorker(context,params,URL,requestMethod,listener);
    }
}
