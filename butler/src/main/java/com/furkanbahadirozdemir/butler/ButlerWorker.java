package com.furkanbahadirozdemir.butler;

import android.content.Context;
import android.os.Debug;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ButlerWorker {

    RequestQueue queue;
    IButlerListener listener;
    Map<String,String> params = new HashMap<String, String>();


    Context context;
    String URL;
    int paramType = -1;

    public ButlerWorker(Context context, Map<String,String> params, String URL, int paramType,IButlerListener listener)
    {

        this.listener = listener;
        this.context = context;
        this.params = Clone(params);
        this.URL = URL;
        this.paramType = paramType;

       Work();

    }


    private void Work() {

        RequestQueue queue = Volley.newRequestQueue(context);


        StringRequest postRequest = new StringRequest(paramType, URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        if (listener != null)
                            listener.OnResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (listener != null)
                            listener.OnError(error.getMessage());

                    }
                }
        ) {
            @Override
            public Map<String, String>  getParams()
            {
                return Clone(params);
            }
        };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);

    }



    private  <K,V> Map<K,V> Clone(Map<K,V> original) {
        Map<K,V> copy = new HashMap<>();

        for (Map.Entry<K,V> entry: original.entrySet()) {
            copy.put(entry.getKey(), entry.getValue());
        }

        return copy;
    }











}
