package com.furkanbahadirozdemir.bottomnav;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;

import androidx.constraintlayout.widget.ConstraintLayout;

public class BottomNavigation {

    Context context;

    public BottomNavigation(Context context)
    {
        this.context = context;
    }

    public void setSelector(LinearLayout selector)
    {
        this.selector = selector;
    }
    public void setBottomNavigation(LinearLayout nav)
    {
        this.nav = nav;
    }
    public void setIcon(ImageView icon1)
    {
        this.icon1 = icon1;
    }
    public void create()
    {
        Animations();
    }


    public void Create(ConstraintLayout mainLayout)
    {
        LinearLayout iconLayout = new LinearLayout(context);
        iconLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,50));

        iconLayout.setOrientation(LinearLayout.HORIZONTAL);

        ImageView icon = new ImageView(context);
        icon.setImageResource(R.drawable.ic_like);
        iconLayout.addView(icon);

        icon = new ImageView(context);
        icon.setImageResource(R.drawable.ic_like);
        iconLayout.addView(icon);

        icon = new ImageView(context);
        icon.setImageResource(R.drawable.ic_like);
        iconLayout.addView(icon);


        mainLayout.addView(iconLayout);
    }
    public void ChangeIcon(View view)
    {
        ChangeSelector(view);
    }
    //Required variable
    LinearLayout selector,nav;
    ImageView icon1;
    View selectedView;
    //Local variable
    private float iconWidth,selectorWidth,navMargin,iconSelectedPosY,iconNormalPosY;

    private void Animations()
    {
        ConstraintLayout.LayoutParams lp=(ConstraintLayout.LayoutParams)nav.getLayoutParams();
        navMargin = lp.leftMargin;
        icon1.post(new Runnable() {
            @Override
            public void run() {
                iconWidth = icon1.getWidth();
                iconSelectedPosY = icon1.getY()-50;
                iconNormalPosY = icon1.getY();
                selector.post(new Runnable() {
                    @Override
                    public void run() {
                        selectorWidth= selector.getWidth();
                        ChangeSelector(icon1);
                    }
                });

            }
        });
    }

    private int getAbsoluteX(View view)
    {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return location[0];
    }
    private float convertPixelsToDp(float px){
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    private void ChangeSelector(final View view)
    {

        if (selectedView != null)
        {
            ObjectAnimator.ofFloat(selectedView, "translationY", iconSelectedPosY,iconNormalPosY).setDuration(200).start();

            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), Color.parseColor("#e5293e"), Color.parseColor("#000000")).setDuration(200);
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    ((ImageView)selectedView).setColorFilter((int) animator.getAnimatedValue());

                }

            });
            colorAnimation.start();
        }



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                float posX = getAbsoluteX(view);
                posX -=convertPixelsToDp(iconWidth)/2;
                posX -=navMargin;

                ObjectAnimator.ofFloat(view, "translationY", iconNormalPosY,iconSelectedPosY).setDuration(200).start();

                ValueAnimator widthAnimator = ValueAnimator.ofInt(selector.getWidth(), (int) posX).setDuration(200);
                final float finalPosX = posX;
                widthAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        int animatedValue = (int) animation.getAnimatedValue();
                        ViewGroup.LayoutParams layoutParams = selector.getLayoutParams();
                        layoutParams.width = animatedValue;
                        selector.setLayoutParams(layoutParams);
                        if ((int) finalPosX == animatedValue)
                        {
                            selectedView = view;
                        }
                    }

                });
                widthAnimator.start();

                ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), Color.parseColor("#000000"), Color.parseColor("#e5293e")).setDuration(200);
                colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                    @Override
                    public void onAnimationUpdate(ValueAnimator animator) {
                        ((ImageView)view).setColorFilter((int) animator.getAnimatedValue());

                    }


                });
                colorAnimation.start();

            }
        }, 0);


    }

}
