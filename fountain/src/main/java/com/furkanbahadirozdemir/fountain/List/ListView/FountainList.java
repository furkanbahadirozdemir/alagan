package com.furkanbahadirozdemir.fountain.List.ListView;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.furkanbahadirozdemir.fountain.Adapter.ListViewAdapter;
import com.furkanbahadirozdemir.fountain.Interface.IListview;
import com.furkanbahadirozdemir.fountain.Interface.IListViewClick;

import java.util.List;

public class FountainList
{


    ListViewAdapter adapter;
    Context context;
    IListview listener;
    IListViewClick listenerClick,listenerLongClick;

    List<?> itemList;

    int Layout;
    ListView listView;

    public FountainList(ListView listView,Context context)
    {
        this.context = context;
        this.listView = listView;
    }


    public FountainList  SetListener(IListview listener) {
        this.listener = listener;
        return this;
    }
    public FountainList SetClickListener(IListViewClick listenerClick)
    {
        this.listenerClick = listenerClick;
        return this;
    }
    public FountainList SetLongClickListener(IListViewClick listenerClick)
    {
        this.listenerLongClick = listenerClick;
        return this;
    }
    public FountainList  SetItemList(List<?> itemList)
    {
        this.itemList = itemList;
        return  this;
    }

    public FountainList SetLayout(int Layout)
    {
        this.Layout = Layout;
        return this;
    }

    public void Create()
    {
        adapter = new ListViewAdapter(context,Layout,itemList,listener);
        if (listenerClick != null)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                listenerClick.OnClick(i,itemList.get(i));
            }
        });
        if (listenerLongClick != null)
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                listenerLongClick.OnClick(i,itemList.get(i));
                return true;
            }
        });
        listView.setAdapter(adapter);
    }

    public void  notifyDataSetChanged()
    {
        adapter.notifyDataSetChanged();
    }

}
