package com.furkanbahadirozdemir.fountain.List.ExpandableList;

import java.util.List;

public class ModelExpandable {

    public Object headerData;
    public List<?> itemList;

    public ModelExpandable(Object headerData,List<?> itemList)
    {
        this.itemList = itemList;
        this.headerData = headerData;
    }
}
