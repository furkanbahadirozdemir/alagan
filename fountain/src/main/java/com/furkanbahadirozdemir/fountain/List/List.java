package com.furkanbahadirozdemir.fountain.List;

import android.content.Context;
import android.widget.ListView;

import com.furkanbahadirozdemir.fountain.List.ExpandableList.FountainExpandableList;
import com.furkanbahadirozdemir.fountain.List.ListView.FountainList;
import com.furkanbahadirozdemir.fountain.List.RecyclerView.FountainRecyclerView;

public class List {

    Context context;
    public List( Context context)
    {
        this.context = context;
    }
    public FountainExpandableList ExpandableListView()
    {
        return new FountainExpandableList();
    }
    public FountainList ListView(ListView listView )
    {
        return new FountainList(listView,context);
    }
    public FountainRecyclerView RecyclerView()
    {
        return new FountainRecyclerView();
    }
}
