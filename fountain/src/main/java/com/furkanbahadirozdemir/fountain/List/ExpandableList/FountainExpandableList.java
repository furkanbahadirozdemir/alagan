package com.furkanbahadirozdemir.fountain.List.ExpandableList;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.furkanbahadirozdemir.fountain.Adapter.ExpandableListAdapter;
import com.furkanbahadirozdemir.fountain.Interface.IExpandableChildClick;
import com.furkanbahadirozdemir.fountain.Interface.IExpandableClick;
import com.furkanbahadirozdemir.fountain.Interface.IExpandableView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class FountainExpandableList {
    ExpandableListAdapter adapter;
    Context context;
    IExpandableView listener;
    IExpandableChildClick childClickListener,childLongClickListener;
    IExpandableClick clickListener,clickLongListener;
    List<?> itemList = new ArrayList<>();
    int childLayout,groupLayout;

    public FountainExpandableList SetContext(Context context)
    {
        this.context = context;
        return  this;
    }
    public FountainExpandableList SetListener(IExpandableView listener) {
        this.listener = listener;
        return this;
    }
    public FountainExpandableList SetItemList(List<?> itemList)
    {
        this.itemList = itemList;

        return  this;
    }
    public FountainExpandableList SetChildLayout(int Layout)
    {
        this.childLayout = Layout;
        return this;
    }
    public FountainExpandableList SetGroupLayout(int Layout)
    {
        this.groupLayout = Layout;
        return this;
    }
    public void Create(final android.widget.ExpandableListView list)
    {
        adapter = new ExpandableListAdapter(context,itemList,listener,childLayout,groupLayout);
        if (childClickListener != null)
        list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition,long id) {
                childClickListener.OnItemClick(groupPosition,childPosition,getChildList(itemList.get(groupPosition)).get(childPosition));
                return true;
            }
        });
        if (clickListener != null)
        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                clickListener.OnItemClick(i,itemList.get(i));
                return true;
            }
        });

        if (childLongClickListener != null || clickLongListener != null)
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (childLongClickListener != null && ExpandableListView.getPackedPositionType(l) == ExpandableListView.PACKED_POSITION_TYPE_CHILD)
                {
                    int groupPosition = ExpandableListView.getPackedPositionGroup(l);
                    int childPosition = ExpandableListView.getPackedPositionChild(l);


                    childLongClickListener.OnItemClick(groupPosition,childPosition,getChildList(itemList.get(groupPosition)).get(childPosition));
                }
                else if (clickLongListener != null && ExpandableListView.getPackedPositionType(l) == ExpandableListView.PACKED_POSITION_TYPE_GROUP)
                {
                    int groupPosition = ExpandableListView.getPackedPositionGroup(l);
                    clickLongListener.OnItemClick(groupPosition,itemList.get(groupPosition));
                }
                return true;
            }
        });

        list.setAdapter(adapter);

    }
    public void SetChildClick(IExpandableChildClick clickListener)
    {
        this.childClickListener = clickListener;
    }
    public void SetChildLongClick(IExpandableChildClick clickListener)
    {
        this.childLongClickListener = clickListener;
    }
    public void SetClick(IExpandableClick clickListener)
    {
        this.clickListener = clickListener;
    }
    public void SetLongClick(IExpandableClick clickListener)
    {
        this.clickLongListener = clickListener;

    }

    public void notifyDataSetChanged()
    {
        adapter.notifyDataSetChanged();
    }

    private List<?> getChildList(Object deneme)
    {
        List<?> response = null;
        for(Field f : deneme.getClass().getDeclaredFields())
        {
            if (f.getType() == List.class)
            {
                f.setAccessible(true);

                try {
                    response = (List<?>) f.get(deneme);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }


            }

        }
        return response;
    }
}
