package com.furkanbahadirozdemir.fountain.List.RecyclerView;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.furkanbahadirozdemir.fountain.Adapter.RecyclerViewAdapter;
import com.furkanbahadirozdemir.fountain.Interface.IListViewClick;
import com.furkanbahadirozdemir.fountain.Interface.IRecyclerView;

import java.util.List;

public class FountainRecyclerView
{
    RecyclerViewAdapter adapter;
    Context context;
    IRecyclerView listener;
    IListViewClick listenerClick,listenerLongClick;

    List<?> itemList;

    int Layout;

    public FountainRecyclerView() {
    }

    public FountainRecyclerView SetContext(Context context)
    {
        this.context = context;
        return  this;
    }
    public FountainRecyclerView SetListener(IRecyclerView listener) {
        this.listener = listener;
        return this;
    }
    public FountainRecyclerView SetClickListener(IListViewClick listenerClick)
    {
        this.listenerClick = listenerClick;
        return this;
    }
    public FountainRecyclerView SetLongClickListener(IListViewClick listenerClick)
    {
        this.listenerLongClick = listenerClick;
        return this;
    }
    public FountainRecyclerView SetItemList(List<?> itemList)
    {
        this.itemList = itemList;
        return  this;
    }

    public FountainRecyclerView SetLayout(int Layout)
    {
        this.Layout = Layout;
        return this;
    }

    public FountainRecyclerView Create(RecyclerView list)
    {
        adapter = new RecyclerViewAdapter(context,Layout,itemList,listener,listenerClick);

        list.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        list.setLayoutManager(linearLayoutManager);
        return this;
    }

    public void  notifyDataSetChanged()
    {
        adapter.notifyDataSetChanged();
    }
}
