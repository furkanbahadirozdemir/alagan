package com.furkanbahadirozdemir.fountain.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.furkanbahadirozdemir.fountain.Dialog.Interface.IDialogClick;
import com.furkanbahadirozdemir.fountain.List.ExpandableList.ModelExpandable;

public class FountainDialog {

    IDialogClick listener;
    String message,tittle,positiveMessage,negativeMessage;
    Context context;

    public FountainDialog SetContext(Context context)
    {
        this.context = context;
        return this;
    }
    public FountainDialog SetMessage(String message)
    {
        this.message = message;
        return this;
    }
    public FountainDialog SetTittle(String tittle)
    {
        this.tittle = tittle;
        return this;
    }
    public FountainDialog SetView()
    {
        return this;
    }
    public FountainDialog SetButtonText(String positiveMessage,String negativeMessage)
    {
        this.positiveMessage = positiveMessage;
        this.negativeMessage = negativeMessage;
        return this;
    }

    public FountainDialog SetOnClick(IDialogClick listener)
    {
        this.listener = listener;
        return this;
    }

    public FountainDialog Create()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(tittle);
        builder.setMessage(message);
        builder.setNegativeButton(negativeMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                if (listener!= null)
                    listener.OnClick(false);
            }
        });
        builder.setPositiveButton(positiveMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                if (listener!= null)
                    listener.OnClick(true);
            }
        });
        builder.show();
        return this;

    }
}
