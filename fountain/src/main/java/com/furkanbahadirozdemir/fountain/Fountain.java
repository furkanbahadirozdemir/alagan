package com.furkanbahadirozdemir.fountain;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.furkanbahadirozdemir.fountain.Dialog.FountainDialog;
import com.furkanbahadirozdemir.fountain.List.List;

public  class  Fountain
{

    Context context;

    public  static  <T> Fountain Initialize(Context context)
    {
        return new Fountain(context);
    }

    public Fountain(Context context) {
        this.context = context;
    }

    public List List()
   {
        return new List(context);
   }
   public FountainDialog Dialog()
   {
       return new FountainDialog();
   }



}
