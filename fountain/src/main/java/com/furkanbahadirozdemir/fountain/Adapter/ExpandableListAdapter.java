package com.furkanbahadirozdemir.fountain.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;


import com.furkanbahadirozdemir.fountain.Interface.IExpandableView;
import com.furkanbahadirozdemir.fountain.List.ExpandableList.ModelExpandable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<?> itemList;
    private IExpandableView listener;
    private int childLayout,groupLayout;

    public ExpandableListAdapter(Context context, List<?> itemList,IExpandableView listener,int childLayout,int groupLayout) {
        this.context = context;
        this.itemList = itemList;
        this.listener = listener;
        this.childLayout = childLayout;
        this.groupLayout = groupLayout;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {


        return  getChildList(itemList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {


        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(childLayout, null);
        listener.OnDrawItem(view,getChildList(itemList.get(groupPosition)).get(childPosition));
        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {

        return getChildList(itemList.get(groupPosition)).size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return itemList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return itemList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        View header  = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(groupLayout, null);
        listener.OnDrawHeader(header,itemList.get(groupPosition));
        return header;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



    private List<?> getChildList(Object deneme)
    {
        List<?> response = null;
        for(Field f : deneme.getClass().getDeclaredFields())
        {
            if (f.getType() == List.class)
            {
                f.setAccessible(true);

                try {
                    response = (List<?>) f.get(deneme);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }


            }

        }
        return response;

    }

}
