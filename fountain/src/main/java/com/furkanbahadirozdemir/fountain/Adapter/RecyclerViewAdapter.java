package com.furkanbahadirozdemir.fountain.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.furkanbahadirozdemir.fountain.Interface.IListViewClick;
import com.furkanbahadirozdemir.fountain.Interface.IRecyclerView;
import com.furkanbahadirozdemir.fountain.R;

import java.util.List;


public class RecyclerViewAdapter extends  RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {


    private List<?> moviesList;
    IRecyclerView listener;
    IListViewClick clickListener;
    Context context;
    int layout;
    LayoutInflater inflater;
    public RecyclerViewAdapter(Context context, int layout, List<?> moviesList, IRecyclerView listener,IListViewClick clickListener) {
        this.moviesList = moviesList;
        this.layout = layout;
        this.listener = listener;
        this.context = context;
        this.clickListener = clickListener;
        inflater = LayoutInflater.from(context);
        Log.d("Adana","S");
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener != null)
                clickListener.OnClick(position,moviesList.get(position));
            }
        });
       listener.OnView( holder.itemView,moviesList.get(position));
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View itemView) {

            super(itemView);
            if (clickListener != null)
            {

            }
        }

    }
}
