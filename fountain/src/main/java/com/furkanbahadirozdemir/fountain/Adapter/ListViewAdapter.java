package com.furkanbahadirozdemir.fountain.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.furkanbahadirozdemir.fountain.Interface.IListview;

import java.util.List;

public class ListViewAdapter extends BaseAdapter {
    private LayoutInflater userInflater;
    private List<?> itemList;
    private IListview listener;

    private int layout;
    public ListViewAdapter(Context activity,int layout, List<?> userList, IListview listener)
    {
        userInflater = (LayoutInflater) activity.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        this.itemList = userList;
        this.listener = listener;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return itemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View lineView  = userInflater.inflate(layout, null);
        listener.OnListViewItem(lineView,itemList.get(i));
        return lineView;
    }



}
