package com.furkanbahadirozdemir.fountain.Interface;

public interface IListViewClick<T> {
    public void OnClick(int position,T data);
}
