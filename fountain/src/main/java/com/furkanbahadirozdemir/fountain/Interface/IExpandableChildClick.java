package com.furkanbahadirozdemir.fountain.Interface;

public interface IExpandableChildClick {

    public void OnItemClick(int groupID,int childID,Object data);
}
