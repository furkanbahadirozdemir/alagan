package com.furkanbahadirozdemir.fountain.Interface;

import android.view.View;

public interface IListview<T>
{
    public void OnListViewItem(View item, T data);
}
