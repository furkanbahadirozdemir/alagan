package com.furkanbahadirozdemir.fountain.Interface;

import android.view.View;

public interface IRecyclerView {
    public void OnView(View view,Object obj);
    public void OnViewHolder(View view);
}
