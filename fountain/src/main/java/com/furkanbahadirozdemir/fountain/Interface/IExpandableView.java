package com.furkanbahadirozdemir.fountain.Interface;

import android.view.View;

public interface IExpandableView {


    public void OnDrawHeader(View layout,Object data);
    public void OnDrawItem(View layout,Object data);
}
